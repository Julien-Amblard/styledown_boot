# Styleguide options

### Head

    <meta name='viewport' content='width=device-width, initial-scale=1' />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link rel='stylesheet' href='./../build/styles.css' />
    <link rel='stylesheet' href='https://cdn.rawgit.com/styledown/styledown/v1.0.2/data/styledown.css' />
    <style>
    .sg-block {
        border-top: solid 1px rgba(0, 0, 0, 0.1);
        padding-left: 20px;
        padding-top: 20px;
        margin: 20px 0;
        background-color: #fff;
    }
    </style>
    

### Body
    h1 MY super Style Guide
    h2 <a href="https://github.com/styledown/styledown/blob/master/docs/" target="_blank">docs styleguide</a> : 
    ul
        li
            <a href="https://github.com/styledown/styledown/blob/master/docs/Format.md" target="_blank">Format</a>
    div.sg-wrapper
        div.sg-sidebar
        div.sg-content
            div#styleguides(sg-content)

    <script src='https://cdn.rawgit.com/styledown/styledown/v1.0.2/data/styledown.js'></script>