const gulp 				= require('gulp');
const postcss 			= require('gulp-postcss');
const rename 			= require('gulp-rename');
const styleDown 		= require('gulp-styledown');
const watch 			= require('gulp-watch');


gulp.task('css', () => {

	gulp.src(['src/main.css'])
		.pipe(postcss([]))
		.pipe(rename('styles.css'))
		.pipe(gulp.dest('build/'));

});




gulp.task('watch', () => {
	gulp.watch('src/*.css', ['css','SD']);
});


gulp.task('SD', () => {

	gulp.src('src/main.css')
		.pipe(postcss([]))
		.pipe(styleDown({
			config : 'docs/config.md',
			filename: 'styleGuide.html'
		}))
		.pipe(gulp.dest('docs/'));


});

gulp.task('default', ['css', 'SD', 'watch']);
